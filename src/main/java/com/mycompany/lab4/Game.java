 package com.mycompany.lab4;
import java.util.Scanner;

public class Game {
    private Table table;
    private String turnx = "X";
    private String turno = "O";
    private Scanner sn = new Scanner(System.in);
    private Player player1;
    private Player player2;
    private String currentPlayer;
    private String winner = "none";

    public Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        currentPlayer = turnx;
        winner = "None";
        table = new Table();
    }

    public void PrintWelcome() {
        System.out.println("Welcome OX Game");
    }

    public void PrintTurn() {
        System.out.println(currentPlayer + "'s Turn!");
    }

    public void getInputNumber() {
        System.out.println("Please input number (1-9) : ");
        int inputn = sn.nextInt();
        table.setCellValue(inputn - 1, currentPlayer);
    }

    public void CheckWinner() {
        String[] currentTable = table.getTable();
        String case0, case1, case2, case3, case4, case5, case6, case7;
        case0 = currentTable[0] + currentTable[1] + currentTable[2];
        case1 = currentTable[3] + currentTable[4] + currentTable[5];
        case2 = currentTable[6] + currentTable[7] + currentTable[8];
        case3 = currentTable[0] + currentTable[3] + currentTable[6];
        case4 = currentTable[1] + currentTable[4] + currentTable[7];
        case5 = currentTable[2] + currentTable[5] + currentTable[8];
        case6 = currentTable[0] + currentTable[4] + currentTable[8];
        case7 = currentTable[2] + currentTable[4] + currentTable[6];

        if (case0.equals("XXX")){
                  winner = turnx;
            } else if (case0.equals("OOO")){
                winner = turno;
            }
              if (case1.equals("XXX")){
                  winner = turnx;
            } else if (case1.equals("OOO")){
                winner = turno;
            }
              if (case2.equals("XXX")){
                  winner = turnx;
            } else if (case2.equals("OOO")){
                winner = turno;
            }
              if (case3.equals("XXX")){
                  winner = turnx;
            } else if (case3.equals("OOO")){
                winner = turno;
            }
              if (case4.equals("XXX")){
                  winner = turnx;
            } else if (case4.equals("OOO")){
                winner = turno;
            }
              if (case5.equals("XXX")){
                  winner = turnx;
            } else if (case5.equals("OOO")){
                winner = turno;
            }
              if (case6.equals("XXX")){
                  winner = turnx;
            } else if (case6.equals("OOO")){
                winner = turno;
            }
              if (case7.equals("XXX")){
                  winner = turnx;
            } else if (case7.equals("OOO")){
                winner = turno;
            }
    }

    public boolean IsGameOver() {
    String[] currentTable = table.getTable();
    for (String cell : currentTable) {
        if (!cell.equals("X") && !cell.equals("O")) {
            return false;
        }
    }
    return true;
}

    public String getWinner() {
        return winner;
    }

    public void PrintWinner() {
        System.out.println("Player " + winner + " wins the game!!!");
    }

    public void switchPlayer() {
        if (currentPlayer.equals(turnx)) {
            currentPlayer = turno;
        } else {
            currentPlayer = turnx;
        }
    }

    public Table getTable() {
        return table;
    }
}
