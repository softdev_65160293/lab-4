package com.mycompany.lab4;

public class Player {
    private int player_id;
    private String player_name;

    public Player(int player_id, String player_name) {
        this.player_id = player_id;
        this.player_name = player_name;
    }

    public String getPlayerName() {
        return player_name;
    }

    public void setPlayerName(String name) {
        this.player_name = name;
    }

    public int getPlayerId() {
        return player_id;
    }

    public void setPlayerId(int id) {
        this.player_id = id;
    }
}
