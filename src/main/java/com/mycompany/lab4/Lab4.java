package com.mycompany.lab4;

public class Lab4 {
    public static void main(String[] args) {
        Player player1 = new Player(1, "Player 1");
        Player player2 = new Player(2, "Player 2");

        Game game = new Game(player1, player2);
        game.PrintWelcome();
        game.getTable().printTable();
        while (!game.IsGameOver()) {
            game.PrintTurn();
            game.getInputNumber();
            game.CheckWinner();
            game.getTable().printTable();
            if(game.getWinner() != "None"){
                game.PrintWinner();
                break;
            } else {
                game.switchPlayer();
            }
        }
    }
}
